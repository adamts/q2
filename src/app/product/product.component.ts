import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product'


@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs:['product']
})
export class ProductComponent implements OnInit {
@Output() deleteEvent = new EventEmitter<Product>();
@Output() editEvent = new EventEmitter<Product>(); 

  product:Product;
  tempProduct:Product = {pid:null,cost:null,categoryId:null,cnamme:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';
  constructor() { }
   

  sendDelete(){
     this.deleteEvent.emit(this.product);
    }

    cancelEdit(){
    this.product.pid = this.tempProduct.pid;
    this.product.cost = this.tempProduct.cost;
    this.product.categoryId = this.tempProduct.categoryId;
    this.product.cnamme = this.tempProduct.cnamme;
    this.editButtonText = 'Edit'; 
  this.toggleEdit();
}

    

     toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
    
     if(this.isEdit){
       this.tempProduct.pid = this.product.pid;
       this.tempProduct.cost = this.product.cost;
       this.tempProduct.categoryId = this.product.categoryId;
       this.tempProduct.cnamme = this.product.cnamme;
     } else {     
       this.editEvent.emit(this.product);
     }
  }
  
   

    

  ngOnInit() {
  }

}
