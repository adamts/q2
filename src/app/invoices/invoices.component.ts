import { Component, OnInit } from '@angular/core';

import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
    .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class InvoicesComponent implements OnInit {

  invoices;
  currentInvoice;

  constructor(private _invoicesService: InvoicesService) {

  }

  select(invoice){
    this.currentInvoice = invoice;
  }

  addInvoice(invoice){
       this. _invoicesService.addInvoice(invoice);


   }

  ngOnInit() {

        this._invoicesService.getInvoices().subscribe(invoicesData => {this.invoices = invoicesData;; console.log(this.invoices)}); //סוגריים מסוסלים כי יש כמה פעולות ולא צריך נקודה פסיק בסוף הפעולה האחרונה


       
  }

}
