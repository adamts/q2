import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {


  invoicesObservable;

  constructor(private af:AngularFire) { }

  addInvoice (invoice){
 this.invoicesObservable.push(invoice);   
 }

 getInvoices(){
    this.invoicesObservable = this.af.database.list('/invoices');
    return this.invoicesObservable;
	}

}
