import { Component, OnInit } from '@angular/core';
import {ProductsService} from './products.service';

@Component({
  selector: 'jce-products',
  templateUrl: './products.component.html',
   styles: [`
    .products li { cursor: default; }
    .products li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class ProductsComponent implements OnInit {

  products;
  currentProducts;

  constructor(private _productsService: ProductsService) {

  }
    select(product){
    this.currentProducts = product;
  }

   deleteProduct(product){
   //this.users.splice(    // we delete this because we want delete from firebase 
   //  this.users.indexOf(user),1 // indexOf(user) is the location and 1 is the quantity
  //  )
 
  this._productsService.deleteProduct1(product);
   }

   editProduct(product){
       this. _productsService.updateProduct(product);


   }



   addProduct(product){
       this. _productsService.addProduct1(product);


   }



   ngOnInit() {

        this._productsService.getProducts().subscribe(productssData => {this.products = productssData;; console.log(this.products)}); //סוגריים מסוסלים כי יש כמה פעולות ולא צריך נקודה פסיק בסוף הפעולה האחרונה


       
  }

}
